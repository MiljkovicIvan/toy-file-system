#pragma once

#include <Windows.h>
#include "part.h"
#include "Monitor.h"
#include "Utilities.h"



class Memory {
public:
	virtual int read(unsigned long, char*) = 0;
	virtual int write(unsigned long, char*) = 0;
	virtual ~Memory();
	virtual void commit();
};

class PartitionWriter : public Memory {
public:
	PartitionWriter(Partition* part);
	int read(unsigned long cluster, char* buffer) override;
	int write(unsigned long cluster, char* buffer) override;
	~PartitionWriter();
private:
	Partition* partition;
	unsigned long num_of_clusters;
};

class ThreadSafePartitionWriter : public Memory {
public:
	ThreadSafePartitionWriter(Memory* partition_writer);
	int read(unsigned long cluster, char* buffer) override;
	int write(unsigned long cluster, char* buffer) override;
	~ThreadSafePartitionWriter();
private:
	Monitor* monitor;
	Memory* memory;
};

class LimitedDataMemory: public Memory{
public:
	LimitedDataMemory(Memory* memory);
	int read(unsigned long cluster, char* buffer) override;
	int write(unsigned long cluster, char* buffer) override;
	~LimitedDataMemory();
private:
	Memory* memory;
};

class LimitedIndexMemory: public Memory{
public:
	LimitedIndexMemory(Memory* memory);
	int read(unsigned long cluster, char* buffer) override;
	int write(unsigned long cluster, char* buffer) override;
	~LimitedIndexMemory();
private:
	Memory* memory;
};

#define moduleCapacity 5

class MemoryModule :public Memory {
public:
	MemoryModule(Memory* partition_writer);
	~MemoryModule();
	int read(unsigned long position, char* source) override;
	int write(unsigned long position, char* destination) override;
	void commit() override;
private:
	char buffer[moduleCapacity][ClusterSize];
	unsigned long* loaded;
	Memory* writer;
	bool* dirty;
	int load(unsigned long);
	int capacity;
	int getClusterEntry(unsigned long cluster);
	LastRecentUsed * last_recent_used;
};

class MemoryMapper : public Memory {
public:
	MemoryMapper(Memory* data, Memory* index, unsigned long index_cluster);
	int read(unsigned long, char*) override;
	int write(unsigned long, char*) override;
	~MemoryMapper();
	void setIndexCluster(unsigned long);
private:
	unsigned long getPhisicalCluster(unsigned long logicalCluster);

	unsigned long index_cluster;
	Memory* data, *index;
};

class Allocator {
public:
	virtual unsigned long allocate() = 0;
	virtual void free(unsigned long) = 0;
	virtual ~Allocator();
};

class ClusterAllocator : public Allocator {
public:
	ClusterAllocator(unsigned long clusters_in_partition, Memory*);
	~ClusterAllocator();
	unsigned long allocate() override;
	void free(unsigned long) override;
	void format();
private:
	CRITICAL_SECTION cs;
	unsigned long clusters_in_partition;
	int loaded, num_of_clusters;
	char* bitvector;
	Memory* memory;
	bool dirty;
	unsigned long total_allocated_clusters;

	void commit();
	void load(unsigned long);
	unsigned long newFreeCluster();
};

class FileAllocator : public Allocator {
public:
	FileAllocator(Allocator*, Memory*, unsigned long, unsigned long);
	unsigned long allocate() override;
	void free(unsigned long) override;
	unsigned long getIndexCluster();
	~FileAllocator();
private:
	unsigned long num_of_index_clusters, num_of_data_clusters, index_cluster;
	Memory* index_memory;
	Allocator* partitionAllocator;

	unsigned long allocateIndexCluster();
	void freeIndexCluster();
	void write(int, int, unsigned long);
	unsigned long read(int, int);
};
