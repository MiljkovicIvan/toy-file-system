#pragma once

#include "Monitor.h"
#include "KernelPartition.h"

class File;

class KernelFS {
public:
	KernelFS();
	char mount(Partition*);
	char unmount(char);
	char readRootDir(char, EntryNum, Directory&);
	char doesExist(char*);
	File* open(char*, char);
	char format(char);
	char deleteFile(char*);
	~KernelFS();
private:
	KernelPartition** partitions;
	Monitor *monitor;
};
