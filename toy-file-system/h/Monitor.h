#pragma once
#include <Windows.h>

class Monitor {
public:
	Monitor();
	void startRead();
	void endRead();
	void startWrite();
	void endWrite();
	virtual ~Monitor();
protected:
	virtual bool conditionReadStart();
	virtual bool conditionReadEnd();
	virtual bool conditionWriteStart();
	virtual bool conditionWriteEnd();
	bool busy;
	int num_readers, num_readers_waiting, num_writers_waiting;
	CONDITION_VARIABLE r, w;
	CRITICAL_SECTION cs;
};

class RWMonitor : public Monitor {
public:
	RWMonitor();
	~RWMonitor();
private:
	bool conditionReadStart() override;
	bool conditionReadEnd() override;
	bool conditionWriteStart() override;
	bool conditionWriteEnd() override;
};

class Flag {
public:
	Flag(bool);
	~Flag( );
	bool get(); 
	void set(bool);
private:
	bool value;
	CRITICAL_SECTION cs;
};