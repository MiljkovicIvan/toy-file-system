#pragma once

#include "Utilities.h"
#include <Windows.h>
#include "Monitor.h"
#include "fs.h"

class FileMetaData {
public:
	FileMetaData(FileName, unsigned long, unsigned long, EntryNum);
	~FileMetaData();
	unsigned long getSize();
	void setSize(unsigned long);
	unsigned long getIndexCluster();
	void setIndexCluster(unsigned long);
	FileName getFileName();
	void setChanged();
	bool getChanged();
	unsigned int getNumOfUsers();
	void incUsers();
	void decUsers();
	Monitor* getFileMonitor();
	Monitor* getMetaDataMonitor();
	friend bool operator==(FileName&, FileName&);
	EntryNum getEntryNum();
	void setEntryNum(EntryNum num);
private:
	unsigned int num_of_users;
	bool changed;
	FileName name;
	unsigned long index_cluster, size;
	EntryNum entry_num;
	Monitor * fileMonitor, *metaDataMonitor;
	CRITICAL_SECTION cs;
};

class OpenFileTable {
public:
	OpenFileTable(int);
	virtual ~OpenFileTable();
	virtual void insertFile(FileMetaData*);
	virtual FileMetaData* getFile(FileName);
	virtual FileMetaData* deleteFile(FileName);
	virtual void format();
private:
	int getHashCode(FileName);

	struct Node {
		FileMetaData* data;
		Node* next;
		Node(FileMetaData*, Node* _next = nullptr);
	};

	struct HashTableEntry {
		Node* head, *tail;
		HashTableEntry();
	};

	HashTableEntry * table;
	int cap;
};

class ThreadSafeOpenFileTable: public OpenFileTable {
public:
	ThreadSafeOpenFileTable(int);
	~ThreadSafeOpenFileTable();
	void insertFile(FileMetaData*) override;
	FileMetaData* getFile(FileName) override;
	FileMetaData* deleteFile(FileName) override;
	void format() override;
private:
	Monitor* m;
};