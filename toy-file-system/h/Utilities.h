#pragma once
#include "fs.h"
#include <Windows.h>


int getBit(char*, int);
void setBit(char*, int, int);

class FileName {
public:
	FileName();
	FileName(char*);
	char name[FNAMELEN],ext[FEXTLEN], partition;
};

class LastRecentUsed {
public:
	LastRecentUsed(int);
	void update(int entry);
	int getEntry();
	~LastRecentUsed();
private:
	struct SwapElem {
		SwapElem* prev, *next;
		int entry;
	};
	SwapElem* head;
};

;