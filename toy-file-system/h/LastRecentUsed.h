#pragma once

class LastRecentUsed {
public:
	LastRecentUsed(int capacity);
	void use(int);
	int get();
private:
	int capacity;

	struct Elem {
		Elem* next;
		int entry;
	};

	Elem* head;
};
