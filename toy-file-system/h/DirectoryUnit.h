#pragma once

#include "fs.h"
#include "Utilities.h"
#include "Monitor.h"
#include "MemoryStuff.h"

struct DirectoryEntry {
	EntryNum num;
	Entry entry;
};

class DirectoryUnit {
public:
	DirectoryUnit(unsigned long, Memory*, Allocator*);
	~DirectoryUnit();
	int readDirectory(EntryNum, Directory&);
	void update(DirectoryEntry);
	DirectoryEntry get(FileName);
	EntryNum write(Entry);
	void remove(EntryNum);
	void format();
private:
	char* buffer;
	int num_of_entries_in_cluster;
	unsigned long index_cluster, size;
	Memory* memory;
	Allocator* partition_handler;
	Monitor* monitor;

	unsigned long getOffset(EntryNum);
};
