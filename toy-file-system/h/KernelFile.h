#pragma once

#include "fs.h"
#include "MemoryStuff.h"
#include "OpenFIleTable.h"

class Memory;
class Allocator;

class KernelFile {
public:
	KernelFile(); // this is needed for UserKernelFile, dummy constructor
	KernelFile(BytesCnt size, BytesCnt position, unsigned long index_cluster, Memory* memory);

	virtual char write(BytesCnt cnt, char* buffer) = 0;
	virtual char truncate() = 0;

	virtual BytesCnt read(BytesCnt cnt, char* buffer);
	virtual char seek(BytesCnt position);
	virtual BytesCnt filePos();
	virtual char eof();
	virtual BytesCnt getFileSize();
	virtual unsigned long getIndexCluster();
	virtual ~KernelFile();
protected:
	BytesCnt pos, size;
	unsigned long index_cluster;
	Memory* index_memory;
	Memory* data_memory;
	MemoryMapper* data_mapper;
	Memory* index;
};

class ReadableKernelFile : public KernelFile {
public:
	ReadableKernelFile(BytesCnt size, unsigned long index_cluster, Memory* memory);
	~ReadableKernelFile();
	char write(BytesCnt, char* buffer) override;
	char truncate() override;
};

class WritableKernelFile : public KernelFile {
public:
	WritableKernelFile(BytesCnt size, BytesCnt position, unsigned long index_memory, Memory* memory, Allocator* allocator);
	~WritableKernelFile();
	char write(BytesCnt, char* buffer) override;
	char truncate() override;
private:
	FileAllocator* fileAllocator;
};

class KernelPartition;

class UserKernelFile : public KernelFile {
public:
	UserKernelFile(KernelFile*, FileMetaData*, KernelPartition*, char);
	~UserKernelFile();
	char write(BytesCnt, char*) override;
	BytesCnt read(BytesCnt, char*) override;
	char seek(BytesCnt) override;
	BytesCnt filePos() override;
	char eof() override;
	BytesCnt getFileSize() override;
	char truncate() override;
	unsigned long getIndexCluster() override;
private:
	KernelPartition* partition;
	char mode;
	FileMetaData* data;
	KernelFile* file;
};
