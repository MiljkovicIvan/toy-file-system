#pragma once

#include "Utilities.h"
#include "OpenFIleTable.h"
#include "KernelFile.h"
#include "DirectoryUnit.h"



class KernelPartition {
public:
	KernelPartition(Partition*);
	char readRootDir(Directory&, EntryNum);
	char doesExist(FileName);
	KernelFile* openFile(FileName, char);
	void format();
	char deleteFile(FileName);
	void closeFile(FileMetaData*, char);
	~KernelPartition();
private:

	Partition * partition;
	OpenFileTable* openFileTable;
	Monitor * monitor, *openCloseDeleteMonitor;
	Flag* formatFlag;
	ClusterAllocator* clusterAllocator;
	DirectoryUnit* directory_unit;

	Memory * memory;
};

class PartitionReaderWriter {
public:
	PartitionReaderWriter(Partition*);
	int readCluster(ClusterNo, char*);
	int writeCluster(ClusterNo, const char*);
private:
	Partition* partition;
	ClusterNo size;
};