#include "Monitor.h"

Monitor::Monitor()
{
	num_readers = 0;
	num_readers_waiting = 0;
	num_writers_waiting = 0;
	busy = false;
	InitializeConditionVariable(&r);
	InitializeConditionVariable(&w);
	InitializeCriticalSection(&cs); 
}

void Monitor::startRead()
{
	EnterCriticalSection(&cs);
	while (conditionReadStart()) {
		num_readers_waiting++;
		SleepConditionVariableCS(&r, &cs, INFINITE);
		num_readers_waiting--;
	}
	num_readers++;
	WakeAllConditionVariable(&r);
	LeaveCriticalSection(&cs);
}

void Monitor::endRead()
{
	EnterCriticalSection(&cs);
	num_readers--;
	if (conditionReadEnd())
		WakeAllConditionVariable(&w);
	LeaveCriticalSection(&cs);
}

void Monitor::startWrite()
{
	EnterCriticalSection(&cs);
	while (conditionWriteStart()) {
		num_writers_waiting++;
		SleepConditionVariableCS(&w, &cs, INFINITE);
		num_writers_waiting--;
	}
	busy = true;
	LeaveCriticalSection(&cs);
}

void Monitor::endWrite()
{
	EnterCriticalSection(&cs);
	busy = false;
	if (conditionWriteEnd()) {
		WakeAllConditionVariable(&w);
	} else {
		WakeAllConditionVariable(&r);
	}
	LeaveCriticalSection(&cs);
}

Monitor::~Monitor()
{
	DeleteCriticalSection(&cs);
}

bool Monitor::conditionReadStart()
{
	return busy;
}

bool Monitor::conditionReadEnd()
{
	return num_readers == 0 && num_readers_waiting == 0;
}

bool Monitor::conditionWriteStart()
{
	return busy || num_readers != 0 || num_readers_waiting != 0;
}

bool Monitor::conditionWriteEnd()
{
	return num_readers_waiting == 0;
}

Flag::Flag(bool v)
{
	value = v;
	InitializeCriticalSection(&cs);
}

Flag::~Flag()
{
	DeleteCriticalSection(&cs);
}

bool Flag::get()
{
	bool return_value;
	EnterCriticalSection(&cs);
	return_value = value;
	LeaveCriticalSection(&cs);
	return return_value;
}

void Flag::set(bool value)
{
	EnterCriticalSection(&cs);
	this->value = value;
	LeaveCriticalSection(&cs);
}

RWMonitor::RWMonitor()
{
}

RWMonitor::~RWMonitor()
{
}

bool RWMonitor::conditionReadStart()
{
	return busy || num_writers_waiting != 0;
}

bool RWMonitor::conditionReadEnd()
{
	return num_writers_waiting != 0 && num_readers == num_writers_waiting;
}

bool RWMonitor::conditionWriteStart()
{
	return busy || num_readers != num_writers_waiting + 1;
}

bool RWMonitor::conditionWriteEnd()
{
	return num_writers_waiting != 0;
}
