#include "MemoryStuff.h"
#include "Monitor.h"
#include "Utilities.h"
#include <cmath>

Memory::~Memory()
{
}

void Memory::commit()
{
}

MemoryMapper::~MemoryMapper()
{
}

void MemoryMapper::setIndexCluster(unsigned long _index_cluster)
{
	this->index_cluster = _index_cluster;
}

PartitionWriter::PartitionWriter(Partition * part)
{
	partition = part;
	num_of_clusters = partition->getNumOfClusters();
}

int PartitionWriter::read(unsigned long cluster, char * buffer)
{
	if (cluster > num_of_clusters)
		return 0;


	partition->readCluster(cluster, buffer);
	return 1;
}

int PartitionWriter::write(unsigned long cluster, char * buffer)
{
	if (cluster > num_of_clusters)
		return 0;

	if (cluster == 3)
		int i = 0;

	partition->writeCluster(cluster, buffer);

	return 1;
}

PartitionWriter::~PartitionWriter()
{
}

MemoryModule::MemoryModule(Memory * partition_writer)
{
	writer = partition_writer;
	dirty = new bool[moduleCapacity];
	loaded = new unsigned long[moduleCapacity];
	for (int i = 0; i < moduleCapacity; i++) {
		dirty[i] = false;
		loaded[i] = -1;
	}
	last_recent_used = new LastRecentUsed(moduleCapacity);
}

MemoryModule::~MemoryModule()
{

	commit();

	delete dirty;
	delete loaded;
	delete last_recent_used;
}

int MemoryModule::read(unsigned long position, char * c) {
	unsigned long clusterNum = position / ClusterSize;
	unsigned long clusterPos = position % ClusterSize;

	int entry = load(clusterNum);

	last_recent_used->update(entry);

	*c = buffer[entry][clusterPos];
	
	return 1;
}

int MemoryModule::load(unsigned long cluster) {
	int entry = getClusterEntry(cluster); // if already loaded

	if(entry == -1) {
		//find entry to put back to disk
		entry = last_recent_used->getEntry();

		if (dirty[entry]) {
			writer->write(loaded[entry], buffer[entry]);
			dirty[entry] = false;
		}

		writer->read(cluster, buffer[entry]);
		loaded[entry] = cluster;
	}

	return entry;
}

int MemoryModule::getClusterEntry(unsigned long cluster)
{
	for (int i = 0; i < moduleCapacity; i++)
		if (loaded[i] == cluster)
			return i;
	return -1;
}

int MemoryModule::write(unsigned long position, char* c) {
	unsigned long clusterNum = position / ClusterSize;
	unsigned long clusterPos = position % ClusterSize;

	int entry = load(clusterNum);

	if (entry == 3) {
		int i;
		i = 1;
	}

	last_recent_used->update(entry);

	buffer[entry][clusterPos] = *c;
	dirty[entry] = true;
	
	return 1;
}

void MemoryModule::commit()
{
	for(int i = 0; i < moduleCapacity; i++)
		if(dirty[i]) {
			writer->write(loaded[i], buffer[i]);
			dirty[i] = false;
		}
}

MemoryMapper::MemoryMapper(Memory* data, Memory * index, unsigned long index_cluster) {
	this->data = data;
	this->index = index;
	this->index_cluster = index_cluster;
}

int MemoryMapper::read(unsigned long cluster, char * buffer)
{
	return data->read(getPhisicalCluster(cluster), buffer);
}

int MemoryMapper::write(unsigned long cluster, char * buffer)
{
	return data->write(getPhisicalCluster(cluster), buffer);
}


unsigned long MemoryMapper::getPhisicalCluster(unsigned long logicalCluster)
{
	unsigned long offset = 0;
	if (logicalCluster < 256) {
		offset = index_cluster*ClusterSize + logicalCluster * sizeof(unsigned long);
	} else {
		int entry = (logicalCluster - 256) / 512;
		unsigned long indexCluster2 = 0;
		unsigned long index_cluster_offset = index_cluster * ClusterSize + (256 + entry) * sizeof(unsigned long);
		char* indexCluster2_ptr = reinterpret_cast<char*>(&indexCluster2);
		for(int i = 0; i < sizeof(unsigned long); i++)
			index->read(index_cluster_offset + i, indexCluster2_ptr + i);
		offset = indexCluster2 * ClusterSize + ((logicalCluster - 256) % 512) * sizeof(unsigned long);
	}

	unsigned long phisicalCluster = 0;
	char* phisicalCluster_ptr = reinterpret_cast<char*>(&phisicalCluster);
		
	for (int i = 0; i < sizeof(unsigned long); i++) {
		index->read(offset + i, phisicalCluster_ptr + i);
	}
	return phisicalCluster;
}

ClusterAllocator::ClusterAllocator(unsigned long _clusters_in_partition, Memory * _memory)
{
	memory = _memory;
	clusters_in_partition = _clusters_in_partition;
	dirty = false;

	//num_of_clusters = (clusters_in_partition + ClusterSize - 1) / ClusterSize;
	num_of_clusters = clusters_in_partition / ClusterSize / 8 + 1;
	loaded = -1;
	bitvector = new char[ClusterSize];

	InitializeCriticalSection(&cs);

	total_allocated_clusters = num_of_clusters;
}

ClusterAllocator::~ClusterAllocator()
{
	commit();
	delete[] bitvector;
	DeleteCriticalSection(&cs);
}

unsigned long ClusterAllocator::allocate()
{
	EnterCriticalSection(&cs);
	unsigned long cluster = newFreeCluster();
	if (cluster != 0) {
		//setBit(bitvector, cluster%ClusterSize, 1);
		setBit(bitvector, cluster%(ClusterSize*8), 1);
		dirty = true;
		total_allocated_clusters++;
	}
	LeaveCriticalSection(&cs);
	return cluster;
}

void ClusterAllocator::free(unsigned long cluster)
{
	EnterCriticalSection(&cs);
	load(cluster / (ClusterSize * 8));
	//setBit(bitvector, cluster % ClusterSize, 0);
	setBit(bitvector, cluster % (ClusterSize*8), 0);
	if (cluster >= num_of_clusters)
		total_allocated_clusters--;
	LeaveCriticalSection(&cs);
}

void ClusterAllocator::format()
{
	memset(bitvector, 0, ClusterSize);

	total_allocated_clusters = num_of_clusters;

	for (int i = 1; i < num_of_clusters; i++) {
		memory->write(i, bitvector);
	}

	for (int i = 0; i < num_of_clusters; i++) {
		setBit(bitvector, i, 1);
	}

	memory->write(0, bitvector);

	loaded = 0;
	dirty = false;
}

void ClusterAllocator::commit()
{
	EnterCriticalSection(&cs);
	if (dirty) {
		memory->write(loaded, bitvector);
		dirty = false;
	}
	LeaveCriticalSection(&cs);
}

void ClusterAllocator::load(unsigned long cluster)
{
	if (loaded != cluster) {
		commit();

		loaded = cluster;
		memory->read(loaded, bitvector);
	}
}

unsigned long ClusterAllocator::newFreeCluster()
{
	if (total_allocated_clusters == clusters_in_partition) {
		return 0;
	}
	if (total_allocated_clusters == 998) {
		int dummy = 1;
	}
	load(0);
	unsigned long ret = 0;
	for (int i = 0; i < num_of_clusters; i++) {
		int limit = 0;
		if (loaded < num_of_clusters-1 || clusters_in_partition % ClusterSize == 0) 
			limit = ClusterSize;
		else
			limit = clusters_in_partition % ClusterSize;

		for (int j = 0; j < limit; j++) {
			if (loaded == 0 && j < num_of_clusters)
				continue; // skip bits for bitvectors

			if (getBit(bitvector, j) == 0) {
				ret = loaded*ClusterSize + j;
				break;
			}
		}

		if (ret != 0)
			break;

		load(loaded + 1);
	}
	
	return ret;
}

FileAllocator::FileAllocator(Allocator * _partition_handler, Memory * _index_memory, unsigned long _num_of_data_clusters, unsigned long _index_cluster)
{
	num_of_data_clusters = _num_of_data_clusters;
	partitionAllocator = _partition_handler;
	index_memory = _index_memory;
	index_cluster = _index_cluster;
	
	if (num_of_data_clusters == 0)
		num_of_index_clusters = 0;
	else if (num_of_data_clusters <= 256)
		num_of_index_clusters = 1;
	else
		num_of_index_clusters = 2 + (num_of_data_clusters - 256 - 1) / 512;
}

unsigned long FileAllocator::allocate()
{
	if (num_of_data_clusters > (ClusterSize/8 + ClusterSize/8 * ClusterSize/4))
		return 0;

	unsigned long ret = 0;

	unsigned long new_num_of_data_clusters = num_of_data_clusters + 1;
	unsigned long new_index_cluster;

	if (new_num_of_data_clusters <= 256)
		new_index_cluster = 1;
	else
		new_index_cluster = 2 + (new_num_of_data_clusters - 256 - 1) / 512;

	if (num_of_index_clusters < new_index_cluster) {
		//allocate new index cluster
		unsigned long allocated = allocateIndexCluster();
		if (allocated == 0) {
			freeIndexCluster();
			return 0;
		}
		ret = 1;
	}

	unsigned long cluster = partitionAllocator->allocate();

	if (cluster == 0) {
		if (ret == 1) {
			freeIndexCluster();
		}
		return 0;
	}
	else
		ret = 2;

	unsigned long index_cluster;
	int entry;
	if (num_of_data_clusters < 256) {
		index_cluster = 0;
		entry = num_of_data_clusters;
	}
	else {
		index_cluster = (num_of_data_clusters - 256) / 512 + 1; // ova racunica nije dobra
		entry = (num_of_data_clusters - 256) % 512;
	}

	write(index_cluster, entry, cluster);
	num_of_data_clusters++;
	return ret;
}

void FileAllocator::free(unsigned long cluster) //cluster is logical
{
	if (num_of_data_clusters == 0)
		return;

	unsigned long ic; //index cluster
	int entry;
	if (cluster < 256) {
		ic = 0;
		entry = cluster;
	} else {
		ic = (cluster - 256) / 512 + 1;
		entry = (cluster - 256) % 512;
	}
	unsigned long real_cluster = read(ic, entry);
	if (real_cluster == 0)
		return;

	write(ic, entry, 0);
	partitionAllocator->free(real_cluster);
	num_of_data_clusters--;

	int current_num_of_index_clusters;
	if (num_of_data_clusters == 0) {
		current_num_of_index_clusters = 0;
	}
	else if (num_of_data_clusters <= 256) {
		current_num_of_index_clusters = 1;
	}
	else {
		current_num_of_index_clusters = 2 + (num_of_data_clusters - 256 - 1) / 512;
	}

	if (current_num_of_index_clusters < num_of_index_clusters) {
		freeIndexCluster();
	}
}

unsigned long FileAllocator::getIndexCluster()
{
	return index_cluster;
}

FileAllocator::~FileAllocator()
{
}

unsigned long FileAllocator::allocateIndexCluster()
{
	unsigned long cluster = partitionAllocator->allocate();
	int ret = 0;
	if (cluster != 0) {
		if (num_of_index_clusters == 0) {
			index_cluster = cluster;
			ret = 2;
		} else {
			write(0, 256 + num_of_index_clusters - 1, cluster);
			ret = 1;
		}
		char fill = 0;
		for (int i = 0; i < ClusterSize; i++)
			index_memory->write(cluster*ClusterSize + i, &fill);
		num_of_index_clusters++;
	}
	return ret;
}

void FileAllocator::freeIndexCluster()
{
	unsigned long cluster = 0;
	if (num_of_index_clusters > 1) {
		cluster = read(0, 256 + num_of_index_clusters - 2);
		write(0, 256 + num_of_index_clusters - 2, 0);
	} else {
	
		cluster = index_cluster;
		index_cluster = 0;
	}
	num_of_index_clusters--;
	partitionAllocator->free(cluster);
}

void FileAllocator::write(int cluster, int entry, unsigned long value)
{
	unsigned long ic = 0; //index cluster

	if (cluster == 0)
		ic = index_cluster;
	else {
		char *ic_ptr = reinterpret_cast<char*>(&ic);
		for (int i = 0; i < sizeof(unsigned long); i++) {
			index_memory->read(index_cluster * ClusterSize + (256 + cluster - 1) * sizeof(unsigned long) + i, ic_ptr + i);
		}
	}

	unsigned long offset = ic * ClusterSize + entry * sizeof(unsigned long);
	char *value_ptr = reinterpret_cast<char*>(&value);
	for (int i = 0; i < sizeof(unsigned long); i++) {
		index_memory->write(offset + i, value_ptr + i);
	}
	//index_memory->commit(); //THIS
}

unsigned long FileAllocator::read(int cluster, int entry)
{
	unsigned long ic = 0;
	if (cluster == 0)
		ic = index_cluster;
	else {
		char *ic_ptr = reinterpret_cast<char*>(&ic);
		for (int i = 0; i < sizeof(unsigned long); i++) {
			index_memory->read(index_cluster * ClusterSize + (256 + cluster - 2) * sizeof(unsigned long) + i, ic_ptr + i);
		}
	}

	unsigned long offset = ic * ClusterSize + entry * sizeof(unsigned long);
	unsigned long value = 0;
	char *value_ptr = reinterpret_cast<char*>(&value);
	for (int i = 0; i < sizeof(unsigned long); i++) {
		index_memory->read(offset + i, value_ptr + i);
	}
	return value;
}

Allocator::~Allocator()
{
}

LimitedDataMemory::LimitedDataMemory(Memory * memory)
{
	this->memory = memory;
}

int LimitedDataMemory::read(unsigned long cluster, char * buffer)
{
	if (cluster == 0) {
		return 1;
	}
	return memory->read(cluster, buffer);
}

int LimitedDataMemory::write(unsigned long cluster, char * buffer)
{
	if (cluster == 0)
		return 1;

	return memory->write(cluster, buffer);
}

LimitedDataMemory::~LimitedDataMemory()
{
}

LimitedIndexMemory::LimitedIndexMemory(Memory * memory)
{
	this->memory = memory;
}

int LimitedIndexMemory::read(unsigned long cluster, char * buffer)
{
	if (cluster == 0)
		memset(buffer, 0, ClusterSize);
	else
		memory->read(cluster, buffer);

	return 1;
}

int LimitedIndexMemory::write(unsigned long cluster, char * buffer)
{
	if (cluster != 0)
		memory->write(cluster, buffer);

	return 1;
}

LimitedIndexMemory::~LimitedIndexMemory()
{
}

ThreadSafePartitionWriter::ThreadSafePartitionWriter(Memory * partition_writer)
{
	memory = partition_writer;
	monitor = new Monitor();
}

int ThreadSafePartitionWriter::read(unsigned long cluster, char * buffer)
{
	monitor->startRead();
	int ret = memory->read(cluster, buffer);
	monitor->endRead();
	return ret;
}

int ThreadSafePartitionWriter::write(unsigned long cluster, char * buffer)
{
	monitor->startWrite();
	int ret = memory->write(cluster, buffer);
	monitor->endWrite();
	return ret;
}

ThreadSafePartitionWriter::~ThreadSafePartitionWriter()
{
	monitor->startWrite();
	delete monitor;
}
