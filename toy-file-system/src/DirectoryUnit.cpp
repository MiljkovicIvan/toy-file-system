#include "DirectoryUnit.h"
#include "KernelFile.h"
#include "MemoryStuff.h"
#include <stdio.h>

class MemoryModule;

DirectoryUnit::DirectoryUnit(unsigned long _index_cluster, Memory * _memory, Allocator * _partition_handler)
{
	index_cluster = _index_cluster;
	memory = _memory;
	partition_handler = _partition_handler;

	num_of_entries_in_cluster = ClusterSize / sizeof(Entry);

	size = ClusterSize / (sizeof(unsigned long) * 2) * ClusterSize + ClusterSize / (sizeof(unsigned long) * 2) * ClusterSize / sizeof(unsigned long) * ClusterSize;
	buffer = new char[sizeof(Entry)];
	memset(buffer, 0, sizeof(Entry));

	monitor = new RWMonitor();
}

DirectoryUnit::~DirectoryUnit()
{
	monitor->startRead();
	monitor->startWrite();

	delete monitor;
	delete[] buffer;
}

int DirectoryUnit::readDirectory(EntryNum num, Directory &d)
{
	monitor->startRead();

	EntryNum current_num = 0;
	KernelFile* file = new ReadableKernelFile(size, index_cluster, memory);
	int directory_index = 0;

	while (true) {
		Entry entry;
		file->read(sizeof(Entry), reinterpret_cast<char*>(&entry));
		if (memcmp(&entry, buffer, sizeof(Entry)) == 0) // if entry is empty
			break;
		current_num++;
		if (current_num % num_of_entries_in_cluster == 0) {
			if (file->seek(file->filePos() + ClusterSize - num_of_entries_in_cluster * sizeof(Entry)) == 0) {
				break;
			}
		}
		if (entry.name[0] == 0)
			continue;

		if (current_num >= num) {
			d[directory_index++] = entry;
			if (directory_index == ENTRYCNT)
				break;
		}
	}

	/*
	while (!file->eof()) {
		Entry entry;
		file->read(sizeof(Entry), reinterpret_cast<char*>(&entry));
		if (memcmp(&entry, buffer, sizeof(Entry)) == 0) {
			monitor->startWrite();
			size = file->filePos() - sizeof(Entry);
			monitor->endWrite();
			break;
		}
		current_num++;
		if (current_num % num_of_entries_in_cluster == 0) {
			if (file->seek(file->filePos() + ClusterSize - num_of_entries_in_cluster * sizeof(Entry)) == 0) {
				break;
			}
		}

		if (entry.name[0] == 0)
			continue;

		if (current_num >= num) {
			d[directory_index++] = entry;
			if (directory_index == ENTRYCNT)
				break;
		}
	}
	*/

	delete file;
	monitor->endRead();
	return directory_index;
}

void DirectoryUnit::update(DirectoryEntry dentry)
{
	monitor->startRead();
	monitor->startWrite();

	KernelFile* file = new WritableKernelFile(size, getOffset(dentry.num), index_cluster, memory, partition_handler);
	dentry.entry.reserved = 'A'; // debug purpose
	char* tmp = reinterpret_cast<char*>(&(dentry.entry));
	file->write(sizeof(Entry), tmp);
	delete file;

	monitor->endWrite();
	monitor->endRead();
}

DirectoryEntry DirectoryUnit::get(FileName fname)
{
	Entry entry;
	bool found = false;

	monitor->startRead();

	KernelFile* file = new ReadableKernelFile(size, index_cluster, memory);
	EntryNum num = 0, current_num = 0;
	
	while (!file->eof() && !found) {
		file->read(sizeof(Entry), reinterpret_cast<char*>(&entry));
		if (memcmp(&entry, buffer, sizeof(Entry)) == 0) {
			monitor->startWrite();
			size = file->filePos() - sizeof(Entry);
			monitor->endWrite();
			break;
		}
		current_num++;

		if (current_num % num_of_entries_in_cluster == 0) {
			if (file->seek(file->filePos() + ClusterSize - num_of_entries_in_cluster * sizeof(Entry)) == 0) {
				break;
			}
		}
		if (entry.name[0] == 0) {
			num++;
			continue;
		}

		if (memcmp(entry.name, fname.name, 8) == 0 &&
			memcmp(entry.ext, fname.ext, 3) == 0) {
			found = true;
			break;
		}
		num++;
	}

	delete file;

	if (!found) {
		entry.name[0] = 0;
	}
	monitor->endRead();

	DirectoryEntry dentry;
	dentry.entry = entry;
	dentry.num = num;
	return dentry;
 }

EntryNum DirectoryUnit::write(Entry entry)
{
	monitor->startRead();
	monitor->startWrite();

	unsigned long current_num = 0;

	KernelFile* file = new WritableKernelFile(size, 0, index_cluster, memory, partition_handler);
	while (!file->eof()) {
		Entry e;
		file->read(sizeof(Entry), reinterpret_cast<char*>(&e));
		if (memcmp(&e, buffer, sizeof(Entry)) == 0) {
			file->truncate();
			file->seek(file->filePos() - sizeof(Entry));
			break;
		}

		if (e.name[0] == 0) {
			file->seek(file->filePos() - sizeof(Entry));
			break;
		}
		current_num++;

		if (current_num % num_of_entries_in_cluster == 0) {
			if (file->seek(file->filePos() + ClusterSize - num_of_entries_in_cluster * sizeof(Entry)) == 0) {
				break;
			}
		}
	}

	EntryNum num = (unsigned long)~0;

	if (file->eof()) {
		if (file->getFileSize() % ClusterSize == num_of_entries_in_cluster * sizeof(Entry)) {
			if (file->write(ClusterSize - num_of_entries_in_cluster * sizeof(Entry), buffer) == 0) {
				return num;
			}
		}

		if (file->getFileSize() % ClusterSize == 0) {
			char b[ClusterSize];
			memset(b, 0, ClusterSize);
			if (file->write(ClusterSize, b) == 0) {
				return num;
			}
			file->seek(file->filePos() - ClusterSize);
		}
	}

	if (file->write(sizeof(Entry), reinterpret_cast<char*>(&entry)) != 0) {
		num = current_num;
		size = file->getFileSize();
	}

	delete file;

	monitor->endWrite();
	monitor->endRead();
	return num;
}

void DirectoryUnit::remove(EntryNum num)
{
	monitor->startRead();
	monitor->startWrite();

	KernelFile *file = new WritableKernelFile(size, getOffset(num), index_cluster, memory, partition_handler);
	Entry entry;
	entry.name[0] = 0;
	file->write(sizeof(Entry), reinterpret_cast<char*>(&entry));
	bool truncate = true;
	if (!file->eof()) {
		if (file->seek(getOffset(num + 1)) != 0) {
			BytesCnt bytesRead = file->read(sizeof(Entry), reinterpret_cast<char*>(&entry));
			if (bytesRead == sizeof(Entry) && memcmp(&entry, buffer, sizeof(Entry)) != 0) {
				truncate = false;
			}
		}
	}

	if (truncate) {
		while (file->seek(getOffset(num - 1))) {
			file->read(sizeof(Entry), reinterpret_cast<char*>(&entry));
			if (entry.name[0] != 0) {
				break;
			}
			num--;
		}

		file->seek(getOffset(num));
		file->truncate();
		size = file->getFileSize();
	}

	delete file;

	monitor->endWrite();
	monitor->endRead();
}

void DirectoryUnit::format()
{
	char b[ClusterSize];
	memset(b, 0, ClusterSize);
	memory->write(index_cluster, b);
}

unsigned long DirectoryUnit::getOffset(EntryNum num)
{
	unsigned long cluster = num / num_of_entries_in_cluster;
	return cluster*ClusterSize + (num % num_of_entries_in_cluster) * sizeof(Entry);
}
