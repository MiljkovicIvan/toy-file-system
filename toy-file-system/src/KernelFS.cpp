#pragma once

#include "KernelFS.h"
#include "Utilities.h"
#include "KernelFile.h"
#include "file.h"

KernelFS::KernelFS()
{
	partitions = new KernelPartition*[26];
	for (int i = 0; i < 26; i++) {
		partitions[i] = nullptr;
	}
	monitor = new Monitor(); 
}

char KernelFS::mount(Partition * partition)
{
	char part_char = 0;

	monitor->startWrite();
	for (int i = 0; i < 26; i++) {
		if (partitions[i] == nullptr) {
			partitions[i] = new KernelPartition(partition);
			part_char = i + 'A';
			break;
		}
	}
	monitor->endWrite();

	return part_char;
}

char KernelFS::unmount(char char_partition)
{
	char ret = 0;
	monitor->startWrite();
	KernelPartition * partition = partitions[char_partition - 'A'];
	partitions[char_partition - 'A'] = nullptr;
	monitor->endWrite();

	if (partition) {
		delete partition;
		ret = 1;
	}
	return ret;
}

char KernelFS::readRootDir(char partition, EntryNum num, Directory& d)
{
	char ret = 0;

	monitor->startRead();

	if (partitions[partition - 'A']) {
		ret = partitions[partition - 'A']->readRootDir(d, num);
	}

	monitor->endRead();

	return ret;
}

char KernelFS::doesExist(char * fname)
{
	int len = strlen(fname);
	char ret = 0;
	if (len < 5 || len > 12)
		return ret;

	FileName name = FileName(fname);

	monitor->startRead();
	if (partitions[name.partition - 'A']) {
		ret = partitions[name.partition - 'A']->doesExist(name);
	}
	monitor->endRead();
	return ret;


}

File * KernelFS::open(char * fname, char mode)
{
	File* file = nullptr;
	FileName name = FileName(fname);
	monitor->startRead();

	if (partitions[name.partition - 'A']) {
		KernelFile * kernelFile = partitions[name.partition - 'A']->openFile(name, mode);
		if (kernelFile) {
			file = new File();
			file->myImpl = kernelFile;
		}
	}
	monitor->endRead();
	return file;
}

char KernelFS::format(char partition)
{
	char ret = 1;

	monitor->startRead();

	if (partitions[partition - 'A']) {
		partitions[partition - 'A']->format();
		ret = 0;
	}

	monitor->endRead();

	return ret;
}

char KernelFS::deleteFile(char * fname)
{
	char ret = 1;
	FileName name = FileName(fname);

	monitor->startRead();
	if (partitions[name.partition - 'A']) {
		ret = partitions[name.partition - 'A']->deleteFile(name);
	}
	monitor->endRead();

	return ret;
}

KernelFS::~KernelFS()
{
	delete[] partitions;
	delete monitor;
}
