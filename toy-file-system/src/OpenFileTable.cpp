#include "OpenFIleTable.h"

bool operator==(FileName & n1, FileName & n2)
{
	bool nameeq = memcmp(n1.name, n2.name, 8) == 0;
	bool exteq = memcmp(n1.ext, n2.ext, 3) == 0;
	return nameeq && exteq;
}

FileMetaData::FileMetaData(FileName _name, unsigned long _index_cluster, unsigned long _size, EntryNum _entry)
{
	name = _name;
	index_cluster = _index_cluster;
	size = _size;
	entry_num = _entry;

	num_of_users = 0;
	changed = false;

	fileMonitor = new Monitor();
	metaDataMonitor = new Monitor();

	InitializeCriticalSection(&cs);
}

FileMetaData::~FileMetaData() {
	delete fileMonitor;
	delete metaDataMonitor;
	DeleteCriticalSection(&cs);
}

unsigned long FileMetaData::getSize() {
	return this->size;
}

void FileMetaData::setSize(unsigned long _size) {
	this->size = _size;
}

unsigned long FileMetaData::getIndexCluster() {
	return this->index_cluster;
}

void FileMetaData::setIndexCluster(unsigned long index) {
	this->index_cluster = index;
}

FileName FileMetaData::getFileName() {
	return this->name;
}

void FileMetaData::setChanged() {
	this->changed = true;
}

bool FileMetaData::getChanged() {
	return this->changed;
}

unsigned int FileMetaData::getNumOfUsers() {
	EnterCriticalSection(&cs);
	unsigned int ret = this->num_of_users;
	LeaveCriticalSection(&cs);
	return ret;
}

void FileMetaData::incUsers() {
	EnterCriticalSection(&cs);
	this->num_of_users++;
	LeaveCriticalSection(&cs);
}

void FileMetaData::decUsers() {
	EnterCriticalSection(&cs);
	this->num_of_users--;
	LeaveCriticalSection(&cs);
}

Monitor * FileMetaData::getFileMonitor()
{
	return fileMonitor;
}

Monitor * FileMetaData::getMetaDataMonitor()
{
	return metaDataMonitor;
}

EntryNum FileMetaData::getEntryNum()
{
	return entry_num;
}

void FileMetaData::setEntryNum(EntryNum num)
{
	entry_num = num;
}

OpenFileTable::Node::Node(FileMetaData * _data, Node * _next) //_next is defaulted to nullptr
{
	data = _data;
	next = _next;
}

OpenFileTable::HashTableEntry::HashTableEntry()
{
	head = tail = nullptr;
}

OpenFileTable::OpenFileTable(int _cap)
{
	cap = _cap;
	table = new HashTableEntry[cap];
	for (int i = 0; i < cap; i++) {
		table[i].head = nullptr;
		table[i].tail = nullptr;
	}
}

OpenFileTable::~OpenFileTable()
{
	delete [] table;
}

void OpenFileTable::insertFile(FileMetaData * fileData)
{
	int entry = getHashCode(fileData->getFileName());

	Node* node = new Node(fileData, nullptr);

	if (table[entry].head == nullptr) {
		table[entry].head = node;
	} else {
		table[entry].tail->next = node;
	}
	table[entry].tail = node;
}

FileMetaData * OpenFileTable::getFile(FileName fileName)
{
	FileMetaData* data = nullptr;
	int entry = getHashCode(fileName);
	Node* head = table[entry].head;
	while (head != nullptr) {
		if (fileName == head->data->getFileName()) {
			data = head->data;
			break;
		} else {
			head = head->next;
		}
	}
	return data;
}

FileMetaData * OpenFileTable::deleteFile(FileName fileName)
{
	FileMetaData* data = nullptr;
	int entry = getHashCode(fileName);
	Node* head = table[entry].head, *prev = nullptr;
	while (head != nullptr) {
		if (fileName == head->data->getFileName()) {
			data = head->data;
			break;
		} else {
			prev = head;
			head = head->next;
		}
	}
	if (head != nullptr) {
		if (prev == nullptr) {
			table[entry].head = table[entry].head->next;
			if (table[entry].head == nullptr) {
				table[entry].tail = nullptr;
			}
		} else {
		
			prev->next = head->next;
			if (head == table[entry].tail) {
				table[entry].tail = prev;
			}
		}
		delete head;
	}

	return data;
}

void OpenFileTable::format()
{
	for (int i = 0; i < cap; i++) {
		Node* cur = table[i].head;
		while (cur) {
			Node* old = cur;
			cur = cur->next;
			delete old->data;
			delete old;
		}
		table[i].head = table[i].tail = nullptr;
	}
}

int OpenFileTable::getHashCode(FileName fileName)
{
	int entry = 0;
	for (int i = 0; i < 8; i++)
		entry += fileName.name[i];

	for (int i = 0; i < 3; i++)
		entry += fileName.ext[i];

	return entry % cap;
}

ThreadSafeOpenFileTable::ThreadSafeOpenFileTable(int cap): OpenFileTable(cap)
{
}

ThreadSafeOpenFileTable::~ThreadSafeOpenFileTable()
{
	delete m;
}

void ThreadSafeOpenFileTable::insertFile(FileMetaData * fileData)
{
	m->startWrite();
	OpenFileTable::insertFile(fileData);
	m->endWrite();
}

FileMetaData * ThreadSafeOpenFileTable::getFile(FileName fileName)
{
	m->startRead();
	FileMetaData* data = OpenFileTable::getFile(fileName);
	m->endRead();
	return data;
}

FileMetaData * ThreadSafeOpenFileTable::deleteFile(FileName fileName)
{
	m->startWrite();
	FileMetaData* data = OpenFileTable::deleteFile(fileName);
	m->endWrite();
	return data;
}

void ThreadSafeOpenFileTable::format()
{
	m->startWrite();
	OpenFileTable::format();
	m->endWrite();
}
