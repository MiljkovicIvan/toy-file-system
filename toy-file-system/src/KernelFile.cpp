#include "KernelFile.h"
#include "KernelPartition.h"

KernelFile::KernelFile()
{
}

KernelFile::KernelFile(
	BytesCnt _size,
	BytesCnt _pos,
	unsigned long _index_cluster,
	Memory* data) //data je obican partition writer { //index je MemoryModule
{	
	size = _size;
	pos = _pos;
	index = new MemoryModule(data); // this is removed from constructor
	index_cluster = _index_cluster;
	index_memory = new LimitedIndexMemory(index);
	data_mapper = new MemoryMapper(new LimitedDataMemory(data), index_memory, index_cluster);
	data_memory = new MemoryModule(data_mapper); // ovo je byte memory module koji se mapira preko indexa
}

KernelFile::~KernelFile() {
	delete data_memory;
	delete data_mapper;
	delete index;
}

char KernelFile::seek(BytesCnt new_pos)
{
	char ret = 0;

	if (new_pos < size) {
		pos = new_pos;
		ret = 1;
	}

	return ret;
}

BytesCnt KernelFile::filePos()
{
	return pos;
}

char KernelFile::eof()
{
	char ret = 0;

	if (pos == size)
		ret = 1;

	return ret;
}

BytesCnt KernelFile::getFileSize()
{
	return size;
}

unsigned long KernelFile::getIndexCluster()
{
	return index_cluster;
}

BytesCnt KernelFile::read(BytesCnt num_bytes, char * buffer)
{
	BytesCnt cnt = 0;
	for (unsigned long i = 0; i < num_bytes && pos < size; i++, pos++) {
		data_memory->read(pos, buffer + i);
		cnt++;
	}
	return cnt;
}

ReadableKernelFile::ReadableKernelFile(
	BytesCnt _size,
	unsigned long _index_cluster,
	Memory * data): KernelFile(_size, 0, _index_cluster, data)
{
}

ReadableKernelFile::~ReadableKernelFile()
{
}

char ReadableKernelFile::write(BytesCnt num_bytes, char * buffer)
{
	return 0;
}

char ReadableKernelFile::truncate()
{
	return 0;
}

WritableKernelFile::WritableKernelFile(
	BytesCnt _size,
	BytesCnt _pos,
	unsigned long _index_cluster,
	Memory * data,
	Allocator* partition_handler):KernelFile(_size, _pos, _index_cluster, data)
{
	this->fileAllocator = new FileAllocator(partition_handler, index, (_size + ClusterSize - 1) / ClusterSize, index_cluster);
}

WritableKernelFile::~WritableKernelFile()
{
	index_memory->commit();
	data_memory->commit();
	delete fileAllocator;
}

char WritableKernelFile::write(BytesCnt num_bytes, char * buffer)
{
	for (BytesCnt i = 0; i < num_bytes; i++, pos++) {
		if (pos == size) {
			if (size % ClusterSize == 0) {
				int ret = fileAllocator->allocate();
				if (ret == 0) {
					return 0;
				}
				else if (ret == 2) { // first data cluster
					index_cluster = fileAllocator->getIndexCluster();
					data_mapper->setIndexCluster(index_cluster);
				}
			}
			size++;
		}
		data_memory->write(pos, buffer + i);
	}

	//data_memory->commit(); //THIS
	return 1;
}

char WritableKernelFile::truncate()
{
	if (size == 0)
		return 1;

	while ((size - 1) / ClusterSize > pos / ClusterSize) {
		fileAllocator->free((size - 1) / ClusterSize);
		size = size - ClusterSize;
	}
	size = pos;
	if (size % ClusterSize == 0) {
		fileAllocator->free(0);
	}

	return 1;
}

UserKernelFile::UserKernelFile(KernelFile * kernel_file, FileMetaData * file_data, KernelPartition * _partition, char _mode)
{
	file = kernel_file;
	data = file_data;
	partition = _partition;
	mode = _mode;
}

UserKernelFile::~UserKernelFile()
{
	bool write = (mode == 'w' || mode == 'W');
	bool append = (mode == 'a' || mode == 'A');

	if (write || append) {
		data->setSize(file->getFileSize());
		data->setIndexCluster(file->getIndexCluster());
		data->setChanged();
	}
	delete file;
	partition->closeFile(data, mode);
}

char UserKernelFile::write(BytesCnt cnt, char * buffer)
{
	return file->write(cnt, buffer);
}

BytesCnt UserKernelFile::read(BytesCnt cnt, char * buffer)
{
	return file->read(cnt, buffer);
}

char UserKernelFile::seek(BytesCnt pos)
{
	return file->seek(pos);
}

BytesCnt UserKernelFile::filePos()
{
	return file->filePos();
}

char UserKernelFile::eof()
{
	return file->eof();
}

BytesCnt UserKernelFile::getFileSize()
{
	return file->getFileSize();
}

char UserKernelFile::truncate()
{
	return file->truncate();
}

unsigned long UserKernelFile::getIndexCluster()
{
	return file->getIndexCluster();
}

