#include "Utilities.h"
#include <string>
#include "fs.h"
#include "MemoryStuff.h"

using namespace std;

int getBit(char* buffer, int pos) {
	int byteNum = pos / 8;
	int bitNum = pos % 8;

	return (buffer[byteNum] >> bitNum) & 1;
}

void setBit(char* buffer, int pos, int value) {
	int byteNum = pos / 8;
	int bitNum = pos % 8;

	char mask = 1 << bitNum;

	if (value == 1) {
		buffer[byteNum] |= mask;
	} else {
		buffer[byteNum] &= ~mask;
	}
}

FileName::FileName()
{
}

FileName::FileName(char * fullname)
{
	partition = *(fullname);

	int len = strlen(fullname);

	for (int i = 0; i < FNAMELEN; i++) {
		name[i] = ' ';
	}
	for (int i = 0; i < FEXTLEN; i++) {
		ext[i] = ' ';
	}

	int extlen = 0;
	int tmp = len;

	memcpy(ext, fullname + len - 3, 3);
	memcpy(name, fullname + 3, len - 3 - 4);
}

LastRecentUsed::LastRecentUsed(int cap)
{
	head = nullptr;
	for (int i = 0; i < cap; i++) {
		SwapElem* elem = new SwapElem();
		elem->prev = nullptr;
		elem->next = nullptr;
		elem->entry = i;

		if (head == nullptr) {
			head = elem;
		}
		else {
			SwapElem* tmp = head; // head is not null
			while (tmp->next)
				tmp = tmp->next;
			tmp->next = elem;
			elem->prev = tmp;
		}
	}
}

void LastRecentUsed::update(int entry)
{
	SwapElem* e = nullptr, *cursor = head;

	while (cursor) {
		if (cursor->entry == entry) {
			e = cursor;
			break;
		}
		cursor = cursor->next;
	}

	SwapElem* tail = head;
	while (tail->next)
		tail = tail->next;

	if (e->next) {
		if (e->prev) {
			e->prev->next = e->next;
			e->next->prev = e->prev;
		} else {
			head = head->next;
			head->prev = nullptr;
			e->next->prev = e->prev;
		}
		e->next = nullptr;
		e->prev = tail;
		tail->next = e;
	}
}

int LastRecentUsed::getEntry()
{
	int ret = head->entry;
	update(head->entry);
	return ret;
}

LastRecentUsed::~LastRecentUsed()
{
	SwapElem* old = head;
	while (head) {
		old = head;
		head = head->next;
		delete old;
	}
}
