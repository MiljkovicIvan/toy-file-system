#include "KernelPartition.h"
#include "KernelFile.h"

KernelPartition::KernelPartition(Partition * partition)
{
	unsigned long num_of_clusters = partition->getNumOfClusters();

	this->partition = partition;
	//memory = new ThreadSafePartitionWriter(new PartitionWriter(partition));
	memory = new PartitionWriter(partition);
	clusterAllocator = new ClusterAllocator(num_of_clusters, memory);
	//directory_unit = new DirectoryUnit((num_of_clusters + ClusterSize - 1) / ClusterSize, memory, clusterAllocator);
	directory_unit = new DirectoryUnit(num_of_clusters/ClusterSize/8 +1 , memory, clusterAllocator);

	openFileTable = new OpenFileTable(5);
	formatFlag = new Flag(false);
	monitor = new Monitor();
	openCloseDeleteMonitor = new RWMonitor();
}

char KernelPartition::readRootDir(Directory& d, EntryNum num)
{
	return directory_unit->readDirectory(num, d);
}

char KernelPartition::doesExist(FileName name)
{
	monitor->startRead();
	openCloseDeleteMonitor->startRead();

	char ret = 1;

	if (openFileTable->getFile(name) == nullptr) {
		DirectoryEntry data = directory_unit->get(name);
		if (data.entry.name[0] == 0) {
			ret = 0;
		}
	}

	openCloseDeleteMonitor->endRead();
	monitor->endRead();
	return ret;
}

KernelFile * KernelPartition::openFile(FileName name, char mode)
{

	if (memcmp(name.name, "fajl2", 5) == 0) {
		int i;
		i = 1;
	}

	if (formatFlag->get())
		return nullptr;

	monitor->startRead();
	openCloseDeleteMonitor->startRead();

	KernelFile* file = nullptr;

	FileMetaData* data = openFileTable->getFile(name);

	if (data == nullptr) {
		openCloseDeleteMonitor->startWrite();

		data = openFileTable->getFile(name);
		if (data == nullptr) {
			DirectoryEntry de = directory_unit->get(name);
			if (de.entry.name[0] != 0) {
				data = new FileMetaData(name, de.entry.indexCluster, de.entry.size, de.num);
				openFileTable->insertFile(data);
			}
			else {
				if (mode == 'w' || mode == 'W') {
					DirectoryEntry dentry;
					memcpy(dentry.entry.name, name.name, 8);
					memcpy(dentry.entry.ext, name.ext, 3);
					dentry.entry.size = 0;
					dentry.entry.indexCluster = 0;
					dentry.num = directory_unit->write(dentry.entry);
					if (dentry.num != (unsigned long)~0) {
						data = new FileMetaData(name, dentry.entry.size, dentry.entry.indexCluster, dentry.num);
						openFileTable->insertFile(data);
					}
				}
			}
		}

		openCloseDeleteMonitor->endWrite();
	}

	if (data != nullptr) {
		data->getMetaDataMonitor()->startRead();
		data->incUsers();
	}

	openCloseDeleteMonitor->endRead();

	if (data == nullptr) {
		monitor->endRead();
		return nullptr;
	}

	KernelFile* kfile = nullptr;
	if (mode == 'w' || mode == 'W') {
		data->getFileMonitor()->startWrite();
		kfile = new WritableKernelFile(data->getSize(), 0, data->getIndexCluster(), memory, clusterAllocator);
		kfile->truncate();
	}
	if (mode == 'r' || mode == 'R') {
		data->getFileMonitor()->startRead();
		kfile = new ReadableKernelFile(data->getSize(), data->getIndexCluster(), memory);
	}
	if (mode == 'a' || mode == 'A') {
		data->getFileMonitor()->startWrite();
		kfile = new WritableKernelFile(data->getSize(), data->getSize(), data->getIndexCluster(), memory, clusterAllocator);
	}

	if (kfile)
		file = new UserKernelFile(kfile, data, this, mode);

	//monitor->endRead();
	return file;
}

void KernelPartition::format()
{
	formatFlag->set(true);
	monitor->startWrite();

	openFileTable->format();
	directory_unit->format();
	clusterAllocator->format();

	monitor->endWrite();
	formatFlag->set(false);
}

char KernelPartition::deleteFile(FileName fname)
{
	char ret = 0;

	monitor->startRead();
	openCloseDeleteMonitor->startRead();
	openCloseDeleteMonitor->startWrite();

	FileMetaData* data = nullptr;

	DirectoryEntry dentry = directory_unit->get(fname);
	if (dentry.entry.name[0] != 0) {
		ret = 1;
		directory_unit->remove(dentry.num);
		data = openFileTable->deleteFile(fname);
	}

	openCloseDeleteMonitor->endWrite();
	openCloseDeleteMonitor->endRead();

	if (data != nullptr) {
		data->getMetaDataMonitor()->startWrite();
		KernelFile* file = new WritableKernelFile(data->getSize(), 0, data->getIndexCluster(), memory, clusterAllocator);
		file->truncate();
		delete file;
		delete data;
	}

	monitor->endRead();

	return ret;
}

void KernelPartition::closeFile(FileMetaData * data, char mode)
{
	openCloseDeleteMonitor->startRead();
	if (mode == 'r' || mode == 'R') {
		data->getFileMonitor()->endRead();
	} else {
		data->getFileMonitor()->endWrite();
	}

	data->decUsers();

	if (data->getNumOfUsers() == 0) {
		FileName fname;
		memcpy(fname.name, data->getFileName().name, 8);
		memcpy(fname.ext, data->getFileName().ext, 8);
		
		openCloseDeleteMonitor->startWrite();

		FileMetaData* file_data = openFileTable->getFile(fname);

		if (file_data != nullptr && file_data->getNumOfUsers() == 0) {

			file_data = openFileTable->deleteFile(fname);
			if (file_data->getChanged()) {
				DirectoryEntry entry;
				entry.num = file_data->getEntryNum();
				memcpy(entry.entry.name, (void*)file_data->getFileName().name, 8);
				memcpy(entry.entry.ext, (void*)file_data->getFileName().ext, 3);
				entry.entry.indexCluster = file_data->getIndexCluster();
				entry.entry.size = file_data->getSize();
				directory_unit->update(entry);
			}
			delete file_data;
			data = nullptr;
		}
		openCloseDeleteMonitor->endWrite();
	}

	if (data)
		data->getMetaDataMonitor()->endRead();

	monitor->endRead();
	openCloseDeleteMonitor->endRead();
}

KernelPartition::~KernelPartition()
{
	monitor->startWrite();

	delete directory_unit;
	delete clusterAllocator;
	delete memory;
	delete openFileTable;
	delete monitor;
	delete openCloseDeleteMonitor;
	delete formatFlag;
}

PartitionReaderWriter::PartitionReaderWriter(Partition * p)
{
	partition = p;
	size = partition->getNumOfClusters();
}

int PartitionReaderWriter::readCluster(ClusterNo num, char * buffer)
{
	if (num >= size || num < 0) {
		return -1;
	}
	else {
		return partition->readCluster(num, buffer);
	}
}

int PartitionReaderWriter::writeCluster(ClusterNo num, const char * buffer)
{
	if (num >= size || num < 0) {
		return -1;
	}
	else {
		return partition->writeCluster(num, buffer);
	}
}
